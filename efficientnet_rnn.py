import torch
from efficientnet import EfficientNet
from torch import nn
from torch.nn import functional as F

class EfficientNetRNN_ver1_gs(nn.Module):
    def __init__(self):
        super(EfficientNetRNN_ver1, self).__init__()
        out_dim = 1
        dr_rate = 0.2
        rnn_hidden_size = 30
        rnn_num_layers = 1
        self.channel_num = 1

        # get a pretrained vgg19 model ( taking only the cnn layers and fine tun them)
        bb = EfficientNet.from_name("efficientnet-b1", in_channels=self.channel_num)
        bb.n_features = 1280
        num_features = 1280
        self.baseModel = bb
        self.dropout = nn.Dropout(dr_rate)
        self.rnn = nn.LSTM(num_features, rnn_hidden_size, rnn_num_layers, batch_first=True)
        self.fc2 = nn.Linear(30, 256)
        self.fc3 = nn.Linear(256, out_dim)

    def forward(self, x):
        batch_size, time_steps, C, H, W = x.size()
        # reshape input  to be (batch_size * timesteps, input_size)
        x = x.contiguous().view(batch_size * time_steps, C, H, W)
        x = self.baseModel(x)
        x = x.flatten(2).mean(dim=-1)
        x = x.view(x.size(0), -1)

        # make output as  ( samples, timesteps, output_size)
        x = x.contiguous().view(batch_size, time_steps, x.size(-1))
        x, (hn, cn) = self.rnn(x)
        x = F.relu(self.fc2(x[:, -1, :])) # get output of the last lstm not full sequence
        x = self.dropout(x)
        x = self.fc3(x)
        return x