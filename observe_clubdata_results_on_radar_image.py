import os
from os.path import join
import json
import torch
import models
from torchvision import transforms
import cv2
from PIL import Image
import matplotlib
from matplotlib import pyplot as plt
matplotlib.use("Qt5Agg")


def get_transformed_PIL_image(cv_image):
    transform = transforms.Compose([transforms.Resize((224, 224)), transforms.ToTensor(),
                                         transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
    tmp = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
    radar_img_pil = Image.fromarray(tmp)
    return transform(radar_img_pil)


radar_images_path = ".." + os.sep + ".." + os.sep + "radar_data" + os.sep + "radar_specs_png"
model_path = join("models_CNN_FC_Radar",  "model_CNN_FC_Radar_tr_0.266_val_0.763.pt")
val_csv = "clubdata_valid.json"

# Load model architecture
model = models.CNN_FC_Radar()

# Load weights
model.load_state_dict(torch.load(model_path))
model.cuda()
# Eval mode
model.eval()

with open(val_csv, 'r') as f:
    val_dataset = json.load(f)

for k in val_dataset:
    # get image
    image_path = join(radar_images_path, k + "_spec.png")
    cv2_img = cv2.imread(image_path)
    img_tranmsformed = get_transformed_PIL_image(cv2_img)[None, :, :, :].cuda()
    pred = model(img_tranmsformed)

    pred_np_speed = pred.cpu().detach().numpy()
    target_speed = val_dataset[k]

    pred_on_img = pred_np_speed * 160/11250 * 224
    target_on_img = target_speed * 160/11250 * 224
    plt.figure()
    plt.imshow(cv2_img, cmap="gray", aspect="auto")
    plt.axhline(pred_on_img, color="red", label="Estimation: {:.4}".format(pred_np_speed[0][0]))
    plt.axhline(target_on_img, color="blue", label="GT: {:.4}".format(target_speed))
    plt.legend()


plt.show()
