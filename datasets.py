from torch.utils.data import Dataset
import torch
import pandas as pd
from utils import *
import numpy as np
import random
import cv2
from matplotlib import pyplot as plt
import matplotlib
matplotlib.use('Qt5Agg')
import json
from torchvision import transforms
from PIL import Image


class GSOnlyDataset(Dataset):
    def __init__(self, gs_cam_dir, clubdata_json):
        self.img_width = 640
        self.img_height = 480
        self.gs_channel_size = 3
        self.gs_cam_dir = gs_cam_dir
        self.valid_gs_list = []
        self.valid_label_list = []
        with open(clubdata_json) as json_file:
            self.clubdata = json.load(json_file)
        self.find_valid_gs_seq()
        self.transform = transforms.Compose([transforms.Resize((224, 224)), transforms.ToTensor()])

    def find_valid_gs_seq(self):
        for corr_riff_key in self.clubdata:
            png_seq = tuple(get_all_files_contains_with_ext(self.gs_cam_dir, corr_riff_key, '.png'))
            if len(png_seq) == 3:
                self.valid_gs_list.append(png_seq)
                self.valid_label_list.append(self.clubdata[corr_riff_key])

    def __len__(self):
        return len(self.valid_gs_list)

    def __getitem__(self, idx):
        img_seq = np.zeros((self.img_height, self.img_width, self.gs_channel_size), dtype="uint8")
        label = np.single(self.valid_label_list[idx])
        # GS Cam images
        for i in range(self.gs_channel_size):
            img_seq[:, :, i] = cv2.imread(self.valid_gs_list[idx][i], cv2.IMREAD_GRAYSCALE)

        tmp = cv2.cvtColor(img_seq, cv2.COLOR_BGR2RGB)
        img_seq_pil = Image.fromarray(tmp)
        img_seq_pil_tr = self.transform(img_seq_pil)
        sample = {'out': img_seq_pil_tr, 'label': label}
        return sample


class RadarOnlyDataset(Dataset):
    def __init__(self, radar_spec_dir, clubdata_json):

        self.img_width = 640
        self.img_height = 480
        self.radar_list = get_all_files_contains_with_ext(radar_spec_dir, 'RIFF', '.png')
        self.valid_radar_list = []
        self.valid_label_list = []
        self.transform = transforms.Compose([transforms.Resize((224, 224)), transforms.ToTensor(),
                                             transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
        with open(clubdata_json) as json_file:
            self.clubdata = json.load(json_file)
        self.find_valid_radar_files()

    def find_valid_radar_files(self):

        for radar_image in self.radar_list:
            corr_riff_key = radar_image.split(os.sep)[-1].split("_spec")[0]
            if corr_riff_key in self.clubdata:
                self.valid_radar_list.append(radar_image)
                self.valid_label_list.append(self.clubdata[corr_riff_key])

    def __len__(self):
        return len(self.valid_radar_list)

    def __getitem__(self, idx):

        radar_spec_path = self.valid_radar_list[idx]
        label = np.single(self.valid_label_list[idx])
        radar_img = cv2.imread(radar_spec_path)
        tmp = cv2.cvtColor(radar_img, cv2.COLOR_BGR2RGB)
        radar_img_pil = Image.fromarray(tmp)
        transformed_radar_img_pil = self.transform(radar_img_pil)
        sample = {'out': transformed_radar_img_pil, 'label': label}
        return sample


class RadarOnlyDataset_npy(Dataset):
    def __init__(self, radar_npy_spec_dir, clubdata_json):

        self.radar_list = get_all_files_contains_with_ext(radar_npy_spec_dir, 'RIFF', '.npy')
        self.valid_radar_list = []
        self.valid_label_list = []
        with open(clubdata_json) as json_file:
            self.clubdata = json.load(json_file)
        self.find_valid_radar_files()

    def find_valid_radar_files(self):

        for radar_mat in self.radar_list:
            corr_riff_key = radar_mat.split(os.sep)[-1].split("_spec")[0]
            if corr_riff_key in self.clubdata:
                self.valid_radar_list.append(radar_mat)
                self.valid_label_list.append(self.clubdata[corr_riff_key])

    def __len__(self):
        return len(self.valid_radar_list)

    def __getitem__(self, idx):

        radar_spec_npy_path = self.valid_radar_list[idx]
        label = np.single(self.valid_label_list[idx])
        radar_mat = np.single(np.load(radar_spec_npy_path))

        sample = {'out': radar_mat, 'label': label}
        return sample


class RadarGSDataset(Dataset):
    def __init__(self, radar_spec_dir, gs_cam_dir, clubdata_json):
        self.img_width = 640
        self.img_height = 480
        self.gs_channel_size = 3
        self.gs_cam_dir = gs_cam_dir
        self.radar_spec_dir = radar_spec_dir
        self.valid_gs_list = []
        self.valid_radar_list = []
        self.valid_label_list = []
        with open(clubdata_json) as json_file:
            self.clubdata = json.load(json_file)
        self.find_valid_radar_and_gs_seq()
        self.transform = transforms.Compose([transforms.Resize((224, 224)), transforms.ToTensor()])

    def find_valid_radar_and_gs_seq(self):
        for corr_riff_key in self.clubdata:
            png_seq_paths = tuple(get_all_files_contains_with_ext(self.gs_cam_dir, corr_riff_key, '.png'))
            radar_seq_path = join(self.radar_spec_dir, corr_riff_key +  "_spec.png")
            if len(png_seq_paths) == 3 and os.path.isfile(radar_seq_path):
                self.valid_gs_list.append(png_seq_paths)
                self.valid_radar_list.append(radar_seq_path)
                self.valid_label_list.append(self.clubdata[corr_riff_key])

    def __len__(self):
        return len(self.valid_label_list)

    def __getitem__(self, idx):
        gs_imgs = np.zeros((self.img_height, self.img_width, self.gs_channel_size), dtype="uint8")
        label = np.single(self.valid_label_list[idx])

        # Read Gs sequence images to PIL & Transform
        for i in range(self.gs_channel_size):
            gs_imgs[:, :, i] = cv2.imread(self.valid_gs_list[idx][i], cv2.IMREAD_GRAYSCALE)
        input_images_pil = Image.fromarray(gs_imgs)
        transformed_gs_imgs = self.transform(input_images_pil)

        # Read radar image & To PIL & Transform
        radar_img = cv2.imread(self.valid_radar_list[idx], cv2.IMREAD_GRAYSCALE)
        radar_img_pil = Image.fromarray(radar_img)
        transformed_radar_img = self.transform(radar_img_pil)

        input = torch.cat((transformed_gs_imgs, transformed_radar_img), 0 )
        sample = {'out': input, 'label': label}
        return sample



if __name__ == '__main__':

    dataset_only_gs = GSOnlyDataset("D:\\RAPSODO\\radar_data\\gs_cams", "clubdata_valid.json")
    # dataset_radargs = RadarGSDataset("D:\\RAPSODO\\radar_data\\rif", "D:\\RAPSODO\\radar_data\\radar_specs_png")
    dataset_radar = RadarOnlyDataset("D:\\RAPSODO\\radar_data\\radar_specs_png", "clubdata_valid.json")
    dataset_radar_gs = RadarGSDataset("D:\\RAPSODO\\radar_data\\radar_specs_png", "D:\\RAPSODO\\radar_data\\gs_cams", "clubdata_valid.json")
    # dataset_radar_npy = RadarOnlyDataset_npy("D:\\RAPSODO\\radar_data\\radar_specs_npy", "clubdata_valid.json")
    a = dataset_only_gs[0]
    b = dataset_radar[0]
    c = dataset_radar_gs[0]
    plt.figure()
    plt.imshow(a['radar_img'], cmap='gray', aspect="auto")
    plt.show()

    # dataset_ex = OnlyGSCamDataset("D:\\RAPSODO\\aok_home_mac\\radar_data\\rif")
