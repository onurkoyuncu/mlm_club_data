import os
from os.path import join

def get_all_files_with_ext(dir, ext):
    file_list = []
    for root, dirs, files in os.walk(dir):
        for file in files:
            if file.endswith(ext):
                file_list.append(os.path.join(root, file))
    return file_list


def get_all_files_contains_with_ext(dir, exp, ext):
    file_list = []
    for root, dirs, files in os.walk(dir):
        for file in files:
            abs_path = os.path.join(root, file)
            if exp in abs_path and file.endswith(ext):
                file_list.append(os.path.join(root, file))
    return file_list


def get_all_directories_contains(dir, exp):
    result = []
    for dir_path, dir_names, file_names in os.walk(dir):
        for dir_name in dir_names:
            if exp in dir_name:
                abs_riff_path = join(dir_path, dir_name )
                abs_riff_gscam_path = join(abs_riff_path, 'gscamOuts')
                if os.path.isdir(abs_riff_gscam_path) and len(os.listdir(abs_riff_gscam_path)) > 10:
                    result.append(join(dir_path, dir_name))
    return result


if __name__ == '__main__':
    dir = "D:\\RAPSODO\\aok_home_mac\\radar_data\\rif"
    folder_list = get_all_directories_contains(dir, 'RIFF')
