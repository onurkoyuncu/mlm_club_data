from utils import *
import pandas as pd
from datetime import datetime

csv_dir = ".." + os.sep + ".." + os.sep + "radar_data" + os.sep + "csv_data" + os.sep + "trackman"
csv_list = get_all_files_with_ext(csv_dir, 'csv')

df = pd.read_csv(csv_list[0], header=1, encoding='unicode_escape')
for i in range(1, len(csv_list)):
    frames = [df, pd.read_csv(csv_list[i], header=1, encoding= 'unicode_escape')[1:]]
    df = pd.concat(frames)

df.to_csv("csv_data" + os.sep + "trackman_combined_{}.csv".format(str(datetime.now()).split()[0]))