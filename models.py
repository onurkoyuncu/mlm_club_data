import torch
from efficientnet import EfficientNet
from torch import nn
from torch.nn import functional as F


class CNN_RNN_GS(nn.Module):
    def __init__(self):
        super().__init__()
        out_dim = 1
        dr_rate = 0.2
        rnn_hidden_size = 128
        rnn_num_layers = 1
        self.channel_num = 1

        # get a pretrained vgg19 model ( taking only the cnn layers and fine tun them)
        bb = EfficientNet.from_name("efficientnet-b1", in_channels=self.channel_num)
        bb.n_features = 1280
        self.backbone = bb
        self.dropout = nn.Dropout(dr_rate)
        self.rnn = nn.LSTM(bb.n_features, rnn_hidden_size, rnn_num_layers, batch_first=True)
        self.fc2 = nn.Linear(128, 256)
        self.fc3 = nn.Linear(256, out_dim)

    def forward(self, x):
        batch_size, time_steps, C, H, W = x[:, :, None, :, :].size()
        # reshape input  to be (batch_size * timesteps, input_size)
        x = x.contiguous().view(batch_size * time_steps, C, H, W)
        x = self.backbone(x)
        x = x.flatten(2).mean(dim=-1)
        x = x.view(x.size(0), -1)

        # make output as  ( samples, timesteps, output_size)
        x = x.contiguous().view(batch_size, time_steps, x.size(-1))
        x, (hn, cn) = self.rnn(x)
        x = F.relu(self.fc2(x[:, -1, :])) # get output of the last lstm not full sequence
        x = self.dropout(x)
        x = self.fc3(x)
        return x


class CNN_FC_GS(nn.Module):
    def __init__(self):
        super().__init__()
        self.channel_num = 3
        # get a pretrained efficient net model ( taking only the cnn layers and fine tun them)
        bb = EfficientNet.from_name("efficientnet-b1", in_channels=self.channel_num)
        bb.n_features = 1280
        self.dropout = nn.Dropout(0.2)
        self.backbone = bb
        self.out_dim = 1
        # self.prev_fc = torch.nn.Linear(bb.n_features, 256, bias=True)
        self.last_fc = torch.nn.Linear(bb.n_features, self.out_dim)


    def forward(self, x):
        x = self.backbone(x)
        x = x.flatten(2).mean(dim=-1)
        x = self.dropout(x)
        out = self.last_fc(x)

        return out


class CNN_FC_Radar(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.channel_num = 3
        # get a pretrained efficient net model ( taking only the cnn layers and fine tun them)
        bb = EfficientNet.from_name("efficientnet-b1", in_channels=self.channel_num)
        bb.n_features = 1280
        self.dropout = nn.Dropout(0.2)
        self.backbone = bb
        self.out_dim = 1
        self.last_fc = torch.nn.Linear(bb.n_features, self.out_dim)

    def forward(self, x):
        x = self.backbone(x)
        x = x.flatten(2).mean(dim=-1)
        # x = F.relu(self.prev_fc(x))
        x = self.dropout(x)
        out = self.last_fc(x)
        return out


class CNN_FC_GS_Radar(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.channel_num = 4
        # get a pretrained efficient net model ( taking only the cnn layers and fine tun them)
        bb = EfficientNet.from_name("efficientnet-b1", in_channels=self.channel_num)
        bb.n_features = 1280
        self.dropout = nn.Dropout(0.2)
        self.backbone = bb
        self.out_dim = 1
        self.last_fc = torch.nn.Linear(bb.n_features, self.out_dim, bias=True)

    def forward(self, x):
        x = self.backbone(x)
        x = x.flatten(2).mean(dim=-1)
        x = self.dropout(x)
        out = self.last_fc(x)
        return out


class CNN_FC_GS_Radar_Feature_Fusion(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.channel_num_gs = 3
        self.channel_num_radar = 1
        # get a pretrained efficient net model ( taking only the cnn layers and fine tun them)
        bb0 = EfficientNet.from_name("efficientnet-b1", in_channels=self.channel_num_gs)
        bb0.n_features = 1280
        bb1 = EfficientNet.from_name("efficientnet-b1", in_channels=self.channel_num_radar)
        bb1.n_features = 1280

        self.dropout = nn.Dropout(0.2)
        self.backbone0 = bb0
        self.backbone1 = bb1
        self.out_dim = 1
        self.last_fc = torch.nn.Linear(bb0.n_features * 2, self.out_dim, bias=True)

    def forward(self, x):
        x0 = x[:, 0:self.channel_num_gs, :, :]
        x1 = x[:, self.channel_num_gs, :, :][:, None, :, :]
        x0 = self.backbone0(x0)
        x1 = self.backbone1(x1)
        x0 = x0.flatten(2).mean(dim=-1)
        x1 = x1.flatten(2).mean(dim=-1)
        x = torch.cat((x0, x1), 1)
        x = self.dropout(x)
        out = self.last_fc(x)
        return out


class RNN_FC_Radar(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.hidden_size = 256
        self.middle_fc_dim = 256
        self.lstm = nn.LSTM(input_size=self.hidden_size,
                            hidden_size=self.hidden_size, bidirectional=True,
                            batch_first=True,
                            num_layers=2)
        self.prev_fc = nn.Linear(self.hidden_size*2, self.middle_fc_dim)
        self.dropout = nn.Dropout(0.2)
        self.final_fc = nn.Linear(self.middle_fc_dim, 1)

    def forward(self, x):
        x, (hn, cn) = self.lstm(x)
        x = F.relu(self.prev_fc(x[:, -1, :]))
        x = self.dropout(x)
        x = self.final_fc(x)
        return x


class CNN_RNN_FC_GS_CNN_FC_Radar(torch.nn.Module):
    def __init__(self):
        super().__init__()
        out_dim = 1
        dr_rate = 0.2
        rnn_hidden_size = 256
        rnn_num_layers = 1
        self.channel_num_1 = 1

        # get a pretrained vgg19 model ( taking only the cnn layers and fine tun them)
        bb0 = EfficientNet.from_name("efficientnet-b1", in_channels=self.channel_num)
        bb0.n_features = 1280
        self.backbone = bb0
        self.dropout = nn.Dropout(dr_rate)
        self.rnn = nn.LSTM(bb0.n_features, rnn_hidden_size, rnn_num_layers, batch_first=True)
        self.fc2 = nn.Linear(128, 256)
        self.fc3 = nn.Linear(256, out_dim)

    def forward(self, x):
        batch_size, time_steps, C, H, W = x[:, :, None, :, :].size()
        # reshape input  to be (batch_size * timesteps, input_size)
        x = x.contiguous().view(batch_size * time_steps, C, H, W)
        x = self.backbone(x)
        x = x.flatten(2).mean(dim=-1)
        x = x.view(x.size(0), -1)

        # make output as  ( samples, timesteps, output_size)
        x = x.contiguous().view(batch_size, time_steps, x.size(-1))
        x, (hn, cn) = self.rnn(x)
        x = F.relu(self.fc2(x[:, -1, :])) # get output of the last lstm not full sequence
        x = self.dropout(x)
        x = self.fc3(x)
        return x