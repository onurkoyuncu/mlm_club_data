import os
from utils import *
from datetime import datetime
import pandas as pd
import time
import numpy as np
import json
import random

riff_and_clubdata_dict_train = dict()
riff_and_clubdata_dict_valid = dict()

riff_dir = ".." + os.sep + ".." + os.sep + "radar_data" + os.sep + "rif" + os.sep + "US"
all_csv_dir = ".." + os.sep + ".." + os.sep + "radar_data" + os.sep + "csv_data" + os.sep + "trackman"

riff_list = get_all_directories_contains(riff_dir, "RIFF")
csv_file = os.path.join("csv_data", "trackman_combined_2022-02-07.csv")
gt_df = pd.read_csv(csv_file, header=0, encoding='unicode_escape')

random.shuffle(riff_list)
no_of_data_in_valid = 70

riff_cnt = 0
for riff_path in riff_list:
    print(riff_cnt)
    # only take directory element containing RIFF
    riff_name = os.path.basename(riff_path)
    day_res, month_res, year_res, hour_res, min_res, sec_res = riff_name.split('_')[1:]
    time_res = datetime(int(year_res), int(month_res), int(day_res),
                        int(hour_res), int(min_res), int(sec_res))
    total_seconds_res = (time_res - datetime(1970, 1, 1)).total_seconds()
    gt_found = False
    for gt_idx, gt_row in gt_df.iterrows():
        if gt_idx == 0:
            continue
        time_info_path_gt = gt_row['Date']
        ts = time.strptime(time_info_path_gt, "%m/%d/%Y %I:%M:%S %p")
        time_gt = datetime(ts.tm_year, ts.tm_mon, ts.tm_mday,
                           ts.tm_hour, ts.tm_min, ts.tm_sec)
        total_seconds_gt = (time_gt - datetime(1970,1,1)).total_seconds()

        if np.abs(total_seconds_res - total_seconds_gt) < 8:
            gt_found = True
            # print(res_row['ClubSpeed'], gt_row['Club Speed'])
            gt_speed = float(gt_row['Club Speed'])
            if riff_cnt < no_of_data_in_valid:
                riff_and_clubdata_dict_valid[riff_name] = gt_speed
            else:
                riff_and_clubdata_dict_train[riff_name] = gt_speed
            riff_cnt += 1
            break
    if not gt_found:
        print(riff_name)
        a = 5
with open("clubdata_valid.json", "w") as outfile:
    json.dump(riff_and_clubdata_dict_valid, outfile, indent=4)

with open("clubdata_train.json", "w") as outfile:
    json.dump(riff_and_clubdata_dict_train, outfile, indent=4)