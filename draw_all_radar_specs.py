import numpy as np
import os
from matplotlib import pyplot as plt
import matplotlib
matplotlib.use('Qt5Agg')

radar_spec_dir = ".." + os.sep + ".." + os.sep + "radar_data" + os.sep + "radar_specs"

radar_specs = os.listdir(radar_spec_dir)

for radar_spec_name in radar_specs:
    radar_spec_path = os.path.join(radar_spec_dir, radar_spec_name)
    radar_spec = np.load(radar_spec_path)
    plt.figure()
    plt.imshow(radar_spec, aspect="auto", cmap="jet")
plt.show()

