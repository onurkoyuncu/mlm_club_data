from datasets import *
from torch.utils.data import DataLoader
from efficientnet import EfficientNet
from torch.optim import Adam
from torch.nn import MSELoss
from models import *
import os
import numpy as np
import torch


def get_avg_loss(dataloader, model):
    loss = 0
    losses = np.zeros(len(dataloader))
    criteria = MSELoss()
    model.eval()
    for idx, sample in enumerate(dataloader):
        img, target_val = sample["out"].cuda(), sample["label"].cuda()
        out_pred_val = model(img)
        losses[idx] = criteria(target_val.unsqueeze(1), out_pred_val).cpu().detach().numpy()
        loss += criteria(target_val.unsqueeze(1), out_pred_val).cpu().detach().numpy()

    return np.mean(np.sqrt(losses))


def train_and_val(dataset_train, dataset_valid,  model, best_models_path, mdl_cfg):
    data_loader_train = DataLoader(dataset_train, batch_size=4, shuffle=True, num_workers=1)
    data_loader_valid = DataLoader(dataset_valid, batch_size=1, shuffle=True, num_workers=1)
    model = model.cuda()
    model.train()

    # get optimizer #
    optimizer = Adam(model.parameters(), lr=0.0001, betas=(0.1, 0.999))
    criteria = MSELoss()
    best_val_loss = np.Inf
    best_tr_loss = np.Inf
    for e in range(150):
        for idx_train, sample_train in enumerate(data_loader_train):
            # print(idx_train)
            img_train, target_train = sample_train["out"].cuda(), sample_train["label"].cuda()
            out_pred = model(img_train)

            loss = criteria(target_train.unsqueeze(1), out_pred)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        val_loss = get_avg_loss(data_loader_valid, model)
        tr_loss = get_avg_loss(data_loader_train, model)
        if val_loss < best_val_loss and tr_loss < 1 and val_loss < 5:
            best_val_loss = val_loss
            # best_tr_loss = tr_loss
            torch.save(model.state_dict(), os.path.join(best_models_path, 'model_{}_tr_{:.3}_val_{:.3}.pt'.format(mdl_cfg, tr_loss, val_loss)))
        print("-----------------------------------------------------------------")
        print("Epoch {}".format(e))
        print("Validation Loss: {}".format(val_loss))
        print("Training Loss: {}\n".format(tr_loss))
        model.train()



if __name__ == '__main__':

    mdl_cfg = 'CNN_FC_GS_Radar_Feature_Fusion'
    riff_dir = ".." + os.sep + ".." + os.sep + "radar_data" + os.sep + "rif"
    # Radar Spectrograms
    radar_spec_dir = ".." + os.sep + ".." + os.sep + "radar_data" + os.sep + "radar_specs_png"
    radar_spec_dir_npy = ".." + os.sep + ".." + os.sep + "radar_data" + os.sep + "radar_specs_npy"

    # Gs Cams
    gs_cam_dir = ".." + os.sep + ".." + os.sep + "radar_data" + os.sep + "gs_cams"
    clubdata_json_train = "clubdata_train.json"
    clubdata_json_valid = "clubdata_valid.json"
    best_models_path = ".\models_gs_radar"

    mdl_cfgs = ["CNN_FC_GS_Radar",
                "CNN_FC_GS_Radar_Feature_Fusion", "CNN_RNN_GS",
                "RNN_FC_Radar"]

    for mdl_cfg in mdl_cfgs:
        best_models_path = "models_" + mdl_cfg
        if not os.path.exists(best_models_path):
            os.makedirs(best_models_path)
        if mdl_cfg == 'CNN_FC_GS':
            dataset_train = GSOnlyDataset(gs_cam_dir, clubdata_json_train)
            dataset_valid = GSOnlyDataset(gs_cam_dir, clubdata_json_valid)
            model = CNN_FC_GS()
        elif mdl_cfg == 'CNN_FC_Radar':
            dataset_train = RadarOnlyDataset(radar_spec_dir, clubdata_json_train)
            dataset_valid = RadarOnlyDataset(radar_spec_dir, clubdata_json_valid)
            model = CNN_FC_Radar()
            # model.load_state_dict(torch.load("models_radar_only" + os.sep + "model_tr_2.1962883964525775_val_1.9267142521380904.pt"))
        elif mdl_cfg == 'CNN_FC_GS_Radar':
            dataset_train = RadarGSDataset(radar_spec_dir, gs_cam_dir, clubdata_json_train)
            dataset_valid = RadarGSDataset(radar_spec_dir, gs_cam_dir, clubdata_json_valid)
            model = CNN_FC_GS_Radar()
        elif mdl_cfg == 'CNN_FC_GS_Radar_Feature_Fusion':
            dataset_train = RadarGSDataset(radar_spec_dir, gs_cam_dir, clubdata_json_train)
            dataset_valid = RadarGSDataset(radar_spec_dir, gs_cam_dir, clubdata_json_valid)
            model = CNN_FC_GS_Radar_Feature_Fusion()
        elif mdl_cfg == 'CNN_RNN_GS':
            dataset_train = GSOnlyDataset(gs_cam_dir, clubdata_json_train)
            dataset_valid = GSOnlyDataset(gs_cam_dir, clubdata_json_valid)
            model = CNN_RNN_GS()
        elif mdl_cfg =='RNN_FC_Radar':
            dataset_train = RadarOnlyDataset_npy(radar_spec_dir_npy, clubdata_json_train)
            dataset_valid = RadarOnlyDataset_npy(radar_spec_dir_npy, clubdata_json_valid)
            model = RNN_FC_Radar()
            # model.load_state_dict(torch.load("models_radar_only" + os.sep + "model_tr_0.847_val_1.0.pt"))
        else:
            raise ValueError('The model configuration value is not correctly set!')

        print("Train and Validation of {}".format(mdl_cfg))
        train_and_val(dataset_train, dataset_valid, model, best_models_path, mdl_cfg)